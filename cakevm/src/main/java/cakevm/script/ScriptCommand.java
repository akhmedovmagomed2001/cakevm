package cakevm.script;

import cakevm.context.ScriptCommandContext;
import cakevm.exception.ExecutionException;

public interface ScriptCommand {
    void execute(ScriptCommandContext context) throws ExecutionException;
}
