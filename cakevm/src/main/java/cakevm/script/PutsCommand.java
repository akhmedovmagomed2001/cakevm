package cakevm.script;

import cakevm.context.ScriptCommandContext;
import cakevm.exception.ExecutionException;
import cakevm.service.CakeVmMemoryService;
import lombok.SneakyThrows;

import java.io.FileWriter;

public class PutsCommand implements ScriptCommand {

    private final CakeVmMemoryService cakeVmMemoryService = CakeVmMemoryService.getInstance();

    @SneakyThrows
    @Override
    public void execute(ScriptCommandContext context) {
        String argument = context.getArgs().get(0);

        String printValue = argument;

        if (!argument.contains("\"")) {
            boolean variableExist = cakeVmMemoryService.isVariableExist(argument);
            if (!variableExist) {
                String errorMessagePattern = "Variable '%s' does not exist in memory";
                String errorMessage = String.format(errorMessagePattern, argument);
                throw new ExecutionException(errorMessage);
            }
            printValue = cakeVmMemoryService.getVariableValue(argument);
        } else {
            printValue = printValue.replace("\"", "");
        }

        FileWriter outputFileWriter = context.getOutputFileWriter();
        outputFileWriter.write(printValue);
        outputFileWriter.write("\n");
    }
}
