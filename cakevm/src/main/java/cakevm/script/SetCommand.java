package cakevm.script;

import cakevm.context.ScriptCommandContext;
import cakevm.service.CakeVmMemoryService;

public class SetCommand implements ScriptCommand {

    private final CakeVmMemoryService cakeVmMemoryService = CakeVmMemoryService.getInstance();

    @Override
    public void execute(ScriptCommandContext context) {
        String variableTitle = context.getArgs().get(0);
        String variableValue = context.getArgs().get(1);

        cakeVmMemoryService.addVariable(variableTitle, variableValue);
    }
}
