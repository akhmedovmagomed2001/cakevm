package cakevm.script;

import cakevm.context.ScriptCommandContext;
import cakevm.exception.ExecutionException;
import cakevm.service.CakeVmMemoryService;

public class DecCommand implements ScriptCommand {

    private final CakeVmMemoryService cakeVmMemoryService = CakeVmMemoryService.getInstance();

    @Override
    public void execute(ScriptCommandContext context) {
        String variableTitle = context.getArgs().get(0);

        boolean variableExist = cakeVmMemoryService.isVariableExist(variableTitle);
        if (!variableExist) {
            String errorMessagePattern = "Variable '%s' does not exist in memory";
            String errorMessage = String.format(errorMessagePattern, variableTitle);
            throw new ExecutionException(errorMessage);
        }

        String variableValueRaw = cakeVmMemoryService.getVariableValue(variableTitle);

        int variableValue = Integer.parseInt(variableValueRaw);
        variableValue--;

        cakeVmMemoryService.addVariable(variableTitle, String.valueOf(variableValue));
    }
}
