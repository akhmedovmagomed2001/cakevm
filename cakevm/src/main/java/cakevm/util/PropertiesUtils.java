package cakevm.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public final class PropertiesUtils {

    public static final String PROPERTY_DOES_NOT_EXIST_MESSAGE_PATTERN = "Property '%s' does not exist";
    private static final Logger LOG = LoggerFactory.getLogger(PropertiesUtils.class);

    private PropertiesUtils() {
    }

    public static Optional<String> getSystemProperty(String propertyTitle) {
        String propertyValue = System.getProperty(propertyTitle);
        return Optional.ofNullable(propertyValue);
    }

    public static boolean isPropertyExist(String propertyTitle) {
        return getSystemProperty(propertyTitle).isPresent();
    }

    public static String getSystemPropertyStrict(String propertyTitle) {
        String errorMessage = getPropertyNotExistsMessage(propertyTitle);
        String propertyValue = getSystemProperty(propertyTitle).orElseThrow(() -> new RuntimeException(errorMessage));

        String logMessagePattern = "Retrieve '%s' property with value '%s'";
        String logMessage = String.format(logMessagePattern, propertyTitle, propertyValue);
        LOG.info(logMessage);

        return propertyValue;
    }

    public static String getPropertyNotExistsMessage(String propertyTitle) {
        return String.format(PROPERTY_DOES_NOT_EXIST_MESSAGE_PATTERN, propertyTitle);
    }

}
