package cakevm.util;

import cakevm.constant.Mode;
import cakevm.constant.ModeOption;
import cakevm.exception.ModeOptionsValidationException;
import cakevm.exception.ModeValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class ModeOptionUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ModeOptionUtils.class);

    private ModeOptionUtils() {
    }

    public static Optional<ModeOption> getModeOptionByCmdOption(String cmdOption) {
        return Arrays.stream(ModeOption.values())
                .filter(modeOption -> modeOption.getCmdOption().equals(cmdOption))
                .findFirst();
    }

    public static List<String> convertModeOptionsToCmd(Set<ModeOption> options) {
        return options.stream()
                .map(ModeOption::getCmdOption)
                .toList();
    }

    public static List<String> argsToOptions(List<String> args) {
        return args.subList(1, args.size());
    }

    public static Map<ModeOption, String> parseRawOptions(List<String> options) throws ModeOptionsValidationException {
        if (options.size() % 2 != 0) {
            String errorMessagePattern = "Invalid options size: %d";
            String errorMessage = String.format(errorMessagePattern, options.size());
            throw new ModeOptionsValidationException(errorMessage);
        }

        Map<String, String> rawOptionsMap = new HashMap<>();
        for (int i = 0; i < options.size(); i += 2) {
            String key = options.get(i).replace("-", "");
            String value = options.get(i + 1);
            rawOptionsMap.put(key, value);
        }

        return rawOptionsMap.keySet().stream().map(ModeOptionUtils::getModeOption)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toMap(Function.identity(), key -> rawOptionsMap.get(key.getCmdOption())));
    }

    private static Optional<ModeOption> getModeOption(String option) {
        Optional<ModeOption> modeOptionByCmdOption = getModeOptionByCmdOption(option);
        if (modeOptionByCmdOption.isEmpty()) {
            LOG.info("Options '{}' is invalid. Skipping it...", option);
        }
        return modeOptionByCmdOption;
    }

    public static void validateOptions(Mode mode, Map<ModeOption, String> optionMap) throws ModeOptionsValidationException {
        Set<ModeOption> expectedOptions = new HashSet<>(mode.getOptions());
        Set<ModeOption> actualOptions = optionMap.keySet();
        boolean isValid = (expectedOptions.containsAll(actualOptions)) && (actualOptions.containsAll(expectedOptions));

        if (!isValid) {
            String messagePattern = "For mode '%s' following options are expected: %s. Input options: %s";
            String modeCmdParameter = mode.getCmdParameter();
            List<String> expectedCmdOptions = convertModeOptionsToCmd(expectedOptions);
            List<String> actualCmdOptions = convertModeOptionsToCmd(actualOptions);
            String errorMessage = String.format(messagePattern, modeCmdParameter, expectedCmdOptions, actualCmdOptions);
            throw new ModeValidationException(errorMessage);
        }
    }
}
