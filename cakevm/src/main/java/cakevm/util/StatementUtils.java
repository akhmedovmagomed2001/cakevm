package cakevm.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class StatementUtils {

    private StatementUtils() {
    }

    public static List<String> extractWords(String statement) {
        List<String> words = new ArrayList<>();
        if (!statement.contains("\"")) {
            List<String> tokens = Arrays.asList(statement.split(" "));
            words.addAll(tokens);
        } else {
            List<String> strings = Arrays.asList(statement.split("\""));
            List<String> firstParts = Arrays.asList(strings.get(0).trim().split(" "));
            words.addAll(firstParts);

            if (strings.size() > 1) {
                String inputStringFormat = "\"%s\"";
                String rawString = strings.get(1);
                String inputString = String.format(inputStringFormat, rawString);
                words.add(inputString);
            } else {
                words.add("\"\"");
            }

            if (strings.size() > 2) {
                List<String> lastParts = Arrays.asList(strings.get(2).trim().split(" "));
                words.addAll(lastParts);
            }
        }
        return words;
    }

}
