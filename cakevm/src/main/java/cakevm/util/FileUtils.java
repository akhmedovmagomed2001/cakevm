package cakevm.util;

import cakevm.exception.FormatCommandException;

import java.io.File;

public final class FileUtils {

    private FileUtils() {
    }

    public static void validateFileNotEmpty(String filePath) {
        File inputFile = new File(filePath);

        if (!inputFile.exists()) {
            String errorMessagePattern = "File does not exist. Filepath: '%s'";
            String errorMessage = String.format(errorMessagePattern, filePath);
            throw new FormatCommandException(errorMessage);
        }

        if (inputFile.length() == 0) {
            String errorMessagePattern = "File is empty. Filepath: '%s'";
            String errorMessage = String.format(errorMessagePattern, filePath);
            throw new FormatCommandException(errorMessage);
        }
    }

    public static void validateFileDoesNotExist(String filePath) {
        File outputFile = new File(filePath);
        if (outputFile.exists()) {
            String errorMessagePattern = "File already exists. Filepath: '%s'";
            String errorMessage = String.format(errorMessagePattern, filePath);
            throw new FormatCommandException(errorMessage);
        }
    }

}
