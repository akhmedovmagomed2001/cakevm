package cakevm.util;

import cakevm.constant.Mode;
import cakevm.exception.ModeValidationException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public final class ModeUtils {

    private ModeUtils() {
    }

    public static void validateMode(String[] args) throws ModeValidationException {
        if (args.length == 0) {
            String errorMessage = "Input arguments is empty. Please specify mode";
            throw new ModeValidationException(errorMessage);
        }

        String inputMode = args[0];
        if (!isValidMode(inputMode)) {
            throw createModeDoesNotExistException(inputMode);
        }
    }

    public static Mode getMode(List<String> args) throws ModeValidationException {
        String inputMode = args.get(0);
        return getModeByCmdParameter(inputMode)
                .orElseThrow(() -> createModeDoesNotExistException(inputMode));
    }

    public static List<String> getAllModes() {
        return Arrays.stream(Mode.values())
                .map(Mode::getCmdParameter)
                .toList();
    }

    public static Optional<Mode> getModeByCmdParameter(String cmdParameter) {
        return Arrays.stream(Mode.values())
                .filter(mode -> mode.getCmdParameter().equals(cmdParameter))
                .findAny();
    }

    private static boolean isValidMode(String cmdArgument) {
        return getAllModes()
                .stream()
                .anyMatch(cmdArgument::equals);
    }

    private static ModeValidationException createModeDoesNotExistException(String inputMode) {
        String errorMessagePattern = "Mode '%s' does not exist. Available modes: %s";
        String availableModes = String.join(", ", getAllModes());
        String errorMessage = String.format(errorMessagePattern, inputMode, availableModes);
        throw new ModeValidationException(errorMessage);
    }
}
