package cakevm;

import cakevm.controller.ModeController;
import cakevm.controller.command.ExecuteCommand;
import cakevm.controller.command.FormatCommand;
import cakevm.controller.command.ValidateCommand;
import cakevm.service.*;
import cakevm.util.ModeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CakeVmApp {

    private static final Logger LOG = LoggerFactory.getLogger(CakeVmApp.class);

    public static void main(String[] args) {
        try {
            startApp(args);
        } catch (Exception ex) {
            String message = ex.getMessage();
            LOG.error(message);
            ex.printStackTrace();

            if (LOG.isDebugEnabled()) {
            }

            System.exit(1);
        }
    }

    private static void startApp(String[] args) {
        init();

        ModeUtils.validateMode(args);
        ModeController.getInstance().process(args);
    }

    private static void init() {
        CakeVmMemoryService.init();
        XmlXsdValidatorService.init();
        FormatService.init();
        PropertiesService.init();

        ConfigValidationService.init();
        CakeVmConfigService.init();
        ScriptValidationService.init();
        ExecutionService.init();

        FormatCommand.init();
        ValidateCommand.init();
        ExecuteCommand.init();

        ModeController.init();
    }
}
