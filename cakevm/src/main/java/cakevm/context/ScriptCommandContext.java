package cakevm.context;

import lombok.Builder;
import lombok.Getter;

import java.io.FileWriter;
import java.util.List;

@Getter
@Builder
public class ScriptCommandContext {
    private final List<String> args;
    private final FileWriter outputFileWriter;
}
