package cakevm.constant;

import java.util.List;

import static cakevm.constant.ModeOption.*;

public enum Mode {

    FORMAT("f", List.of(INPUT, OUTPUT)),
    VALIDATE("v", List.of(INPUT)),
    COMPILE("c", List.of(INPUT, OUTPUT)),
    EXECUTE("e", List.of(INPUT, OUTPUT, TABLE));

    private final String cmdParameter;
    private final List<ModeOption> options;

    Mode(String cmdParameter, List<ModeOption> options) {
        this.cmdParameter = cmdParameter;
        this.options = options;
    }

    public String getCmdParameter() {
        return cmdParameter;
    }

    public List<ModeOption> getOptions() {
        return options;
    }

}
