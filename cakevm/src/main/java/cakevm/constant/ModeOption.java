package cakevm.constant;

public enum ModeOption {

    INPUT("i"),
    OUTPUT("o"),
    CONFIG("c"),
    CONFIG_SCHEMA("cs"),
    TABLE("t");

    private final String cmdOption;

    ModeOption(String cmdOption) {
        this.cmdOption = cmdOption;
    }

    public String getCmdOption() {
        return cmdOption;
    }
}
