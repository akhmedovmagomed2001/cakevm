package cakevm.constant;

public final class SystemProperties {

    private SystemProperties() {
    }

    public static final String CONFIG_PATH_PROPERTY = "cakeConfigPath";
    public static final String CONFIG_SCHEMA_PATH_PROPERTY = "cakeConfigSchemaPath";

}
