package cakevm.controller;

import cakevm.constant.Mode;
import cakevm.constant.ModeOption;
import cakevm.controller.command.*;
import cakevm.util.ModeOptionUtils;
import cakevm.util.ModeUtils;

import java.util.*;
import java.util.function.Consumer;

public class ModeController {

    private final Map<Mode, Consumer<Map<ModeOption, String>>> modeMap = new EnumMap<>(Mode.class);

    private static ModeController instance;

    private ModeCommand formatCommand;
    private ModeCommand validateCommand;
    private ModeCommand compileCommand;
    private ModeCommand executeCommand;

    private ModeController() {
    }

    public void process(String[] args) {
        List<String> argsList = Arrays.asList(args);
        Mode mode = ModeUtils.getMode(argsList);
        Map<ModeOption, String> optionMap = getOptionMap(argsList, mode);
        Consumer<Map<ModeOption, String>> nullableConsumer = modeMap.get(mode);
        Optional.ofNullable(nullableConsumer).orElseThrow().accept(optionMap);
    }

    private static Map<ModeOption, String> getOptionMap(List<String> argsList, Mode mode) {
        List<String> rawOptions = ModeOptionUtils.argsToOptions(argsList);
        Map<ModeOption, String> optionMap = ModeOptionUtils.parseRawOptions(rawOptions);
        ModeOptionUtils.validateOptions(mode, optionMap);
        return optionMap;
    }

    public static void init() {
        ModeController modeController = new ModeController();

        modeController.formatCommand = FormatCommand.getInstance();
        modeController.validateCommand = ValidateCommand.getInstance();
        modeController.compileCommand = new CompileCommand();
        modeController.executeCommand = ExecuteCommand.getInstance();

        modeController.modeMap.put(Mode.FORMAT, modeController.formatCommand::execute);
        modeController.modeMap.put(Mode.VALIDATE, modeController.validateCommand::execute);
        modeController.modeMap.put(Mode.COMPILE, modeController.compileCommand::execute);
        modeController.modeMap.put(Mode.EXECUTE, modeController.executeCommand::execute);

        instance = modeController;
    }

    public static ModeController getInstance() {
        return instance;
    }

}
