package cakevm.controller.command;

import cakevm.constant.ModeOption;
import cakevm.constant.SystemProperties;
import cakevm.service.*;
import cakevm.util.FileUtils;

import java.util.Map;

public class ExecuteCommand implements ModeCommand {

    private PropertiesService propertiesService;
    private FormatService formatService;
    private ConfigValidationService configValidationService;
    private ScriptValidationService scriptValidationService;
    private ExecutionService executionService;

    private static ExecuteCommand instance;

    @Override
    public void execute(Map<ModeOption, String> optionsMap) {
        validateFiles(optionsMap);

        String inputFilepath = optionsMap.get(ModeOption.INPUT);

        configValidationService.validateScriptConfigs();

        String formattedScriptFilepath = formatService.formatScript(inputFilepath);
        scriptValidationService.validateScript(formattedScriptFilepath);

        String outputFilepath = optionsMap.get(ModeOption.OUTPUT);
        executionService.executeScriptFile(formattedScriptFilepath, outputFilepath);
    }

    private void validateFiles(Map<ModeOption, String> optionsMap) {
        String inputFilepath = optionsMap.get(ModeOption.INPUT);
        FileUtils.validateFileNotEmpty(inputFilepath);

        String configFilepath = propertiesService.getProperty(SystemProperties.CONFIG_PATH_PROPERTY);
        FileUtils.validateFileNotEmpty(configFilepath);

        String configSchemaFilepath = propertiesService.getProperty(SystemProperties.CONFIG_SCHEMA_PATH_PROPERTY);
        FileUtils.validateFileNotEmpty(configSchemaFilepath);

        String outputFilepath = optionsMap.get(ModeOption.OUTPUT);
        FileUtils.validateFileDoesNotExist(outputFilepath);
    }

    public static void init() {
        ExecuteCommand executeCommand = new ExecuteCommand();

        executeCommand.propertiesService = PropertiesService.getInstance();
        executeCommand.formatService = FormatService.getInstance();
        executeCommand.configValidationService = ConfigValidationService.getInstance();
        executeCommand.scriptValidationService = ScriptValidationService.getInstance();
        executeCommand.executionService = ExecutionService.getInstance();

        instance = executeCommand;
    }

    public static ExecuteCommand getInstance() {
        return instance;
    }

}
