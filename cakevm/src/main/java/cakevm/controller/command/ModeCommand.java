package cakevm.controller.command;

import cakevm.constant.ModeOption;

import java.util.Map;

public interface ModeCommand {
    void execute(Map<ModeOption, String> optionsMap);
}
