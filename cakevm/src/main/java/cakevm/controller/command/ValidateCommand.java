package cakevm.controller.command;

import cakevm.constant.ModeOption;
import cakevm.constant.SystemProperties;
import cakevm.service.ConfigValidationService;
import cakevm.service.FormatService;
import cakevm.service.PropertiesService;
import cakevm.service.ScriptValidationService;
import cakevm.util.FileUtils;

import java.util.Map;

public class ValidateCommand implements ModeCommand {

    private PropertiesService propertiesService;
    private ScriptValidationService scriptValidationService;
    private ConfigValidationService configValidationService;
    private FormatService formatService;

    private static ValidateCommand instance;

    @Override
    public void execute(Map<ModeOption, String> optionsMap) {
        validateFiles(optionsMap);
        configValidationService.validateScriptConfigs();

        String inputFilepath = optionsMap.get(ModeOption.INPUT);
        String formattedScriptFilepath = formatService.formatScript(inputFilepath);

        scriptValidationService.validateScript(formattedScriptFilepath);
    }

    private void validateFiles(Map<ModeOption, String> optionsMap) {
        String inputFilepath = optionsMap.get(ModeOption.INPUT);
        FileUtils.validateFileNotEmpty(inputFilepath);

        String configFilepath = propertiesService.getProperty(SystemProperties.CONFIG_PATH_PROPERTY);
        FileUtils.validateFileNotEmpty(configFilepath);

        String configSchemaFilepath = propertiesService.getProperty(SystemProperties.CONFIG_SCHEMA_PATH_PROPERTY);
        FileUtils.validateFileNotEmpty(configSchemaFilepath);
    }

    public static void init() {
        ValidateCommand validateCommand = new ValidateCommand();

        validateCommand.propertiesService = PropertiesService.getInstance();
        validateCommand.scriptValidationService = ScriptValidationService.getInstance();
        validateCommand.configValidationService = ConfigValidationService.getInstance();
        validateCommand.formatService = FormatService.getInstance();

        instance = validateCommand;
    }

    public static ValidateCommand getInstance() {
        return instance;
    }

}
