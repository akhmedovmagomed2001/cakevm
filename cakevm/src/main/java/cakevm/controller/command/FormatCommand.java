package cakevm.controller.command;

import cakevm.constant.ModeOption;
import cakevm.service.FormatService;
import cakevm.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class FormatCommand implements ModeCommand {

    private static final Logger LOG = LoggerFactory.getLogger(FormatCommand.class);

    private FormatService formatService;

    private static FormatCommand instance;

    @Override
    public void execute(Map<ModeOption, String> optionsMap) {
        validateFiles(optionsMap);

        String inputFilePath = optionsMap.get(ModeOption.INPUT);
        String outputFilePath = optionsMap.get(ModeOption.OUTPUT);

        formatService.formatScript(inputFilePath, outputFilePath);

        LOG.info("The input file '{}' is successfully formatted", inputFilePath);
        LOG.info("The result can be found here: {}", outputFilePath);
    }

    private void validateFiles(Map<ModeOption, String> optionsMap) {
        String inputFilePath = optionsMap.get(ModeOption.INPUT);
        FileUtils.validateFileNotEmpty(inputFilePath);

        String outputFilePath = optionsMap.get(ModeOption.OUTPUT);
        FileUtils.validateFileDoesNotExist(outputFilePath);
    }

    public static void init() {
        FormatCommand formatCommand = new FormatCommand();

        formatCommand.formatService = FormatService.getInstance();

        instance = formatCommand;
    }

    public static FormatCommand getInstance() {
        return instance;
    }

}
