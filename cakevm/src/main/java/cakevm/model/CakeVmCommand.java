package cakevm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "command")
@XmlAccessorType(XmlAccessType.FIELD)
public class CakeVmCommand implements Serializable {

    @XmlElement(name = "title")
    private String title;
    @XmlElement(name = "args")
    private int argsCount;
    @XmlElement(name = "class")
    private String classPath;
    @XmlElement(name = "description")
    private String description;

}
