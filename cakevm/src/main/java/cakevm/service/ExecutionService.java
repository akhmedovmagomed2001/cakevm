package cakevm.service;

import cakevm.context.ScriptCommandContext;
import cakevm.exception.ExecutionException;
import cakevm.exception.ScriptValidationServiceException;
import cakevm.script.*;
import cakevm.util.StatementUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

public class ExecutionService {

    private static final Logger LOG = LoggerFactory.getLogger(ExecutionService.class);

    private ScriptCommand addCommand;
    private ScriptCommand decCommand;
    private ScriptCommand incCommand;
    private ScriptCommand multCommand;
    private ScriptCommand putsCommand;
    private ScriptCommand setCommand;
    private ScriptCommand subCommand;

    private static ExecutionService instance;

    private Map<String, Consumer<ScriptCommandContext>> scriptCommandsMap;

    public void executeScriptFile(String formattedScriptFilepath, String outputFilepath) {
        File formatterScriptFile = new File(formattedScriptFilepath);
        File outputFile = new File(outputFilepath);

        try (
                Scanner scanner = new Scanner(formatterScriptFile);
                FileWriter fileWriter = new FileWriter(outputFile);
        ) {
            String statementDelimiter = ";";
            scanner.useDelimiter(statementDelimiter);

            int lineCounter = -1;
            while (scanner.hasNext()) {
                lineCounter++;
                String statement = scanner.next().trim();

                if (statement.isBlank()) {
                    continue;
                }

                executeStatement(statement, fileWriter, lineCounter);
            }
        } catch (IOException ex) {
            throw new ScriptValidationServiceException(ex);
        }

        LOG.info("Execution is done");
    }

    private void executeStatement(String statement, FileWriter fileWriter, int lineNumber) {
        List<String> words = StatementUtils.extractWords(statement);

        String command = words.get(0);
        List<String> commandArgs = words.subList(1, words.size());

        ScriptCommandContext scriptCommandContext = ScriptCommandContext.builder()
                .outputFileWriter(fileWriter)
                .args(commandArgs)
                .build();

        try {
            Optional.ofNullable(scriptCommandsMap.get(command))
                    .orElseThrow()
                    .accept(scriptCommandContext);
        } catch (Exception ex) {
            String errorMessagePattern = "Exception occurred on line %d";
            String errorMessage = String.format(errorMessagePattern, lineNumber);
            throw new ExecutionException(errorMessage, ex);
        }
    }

    private void setupScriptCommandMap() {
        scriptCommandsMap = new HashMap<>();

        scriptCommandsMap.put("add", addCommand::execute);
        scriptCommandsMap.put("dec", decCommand::execute);
        scriptCommandsMap.put("inc", incCommand::execute);
        scriptCommandsMap.put("mult", multCommand::execute);
        scriptCommandsMap.put("puts", putsCommand::execute);
        scriptCommandsMap.put("set", setCommand::execute);
        scriptCommandsMap.put("sub", subCommand::execute);
    }

    public static void init() {
        ExecutionService executionService = new ExecutionService();

        executionService.addCommand = new AddCommand();
        executionService.decCommand = new DecCommand();
        executionService.incCommand = new IncCommand();
        executionService.multCommand = new MultCommand();
        executionService.putsCommand = new PutsCommand();
        executionService.setCommand = new SetCommand();
        executionService.subCommand = new SubCommand();

        executionService.setupScriptCommandMap();

        instance = executionService;
    }

    public static ExecutionService getInstance() {
        return instance;
    }
}
