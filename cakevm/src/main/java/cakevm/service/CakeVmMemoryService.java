package cakevm.service;

import cakevm.exception.MemoryException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CakeVmMemoryService {

    private Map<String, String> memoryTable;

    private static CakeVmMemoryService instance;

    public boolean isVariableExist(String variableTitle) {
        return Optional.ofNullable(memoryTable.get(variableTitle)).isPresent();
    }

    public String getVariableValue(String variableTitle) {
        String valueNullable = memoryTable.get(variableTitle);
        String errorMessagePattern = "Variable '%s' does not exist in memory";
        String errorMessage = String.format(errorMessagePattern, variableTitle);
        return Optional.ofNullable(valueNullable).orElseThrow(() -> new MemoryException(errorMessage));
    }

    public void addVariable(String variableTitle, String variableValue) {
        memoryTable.put(variableTitle, variableValue);
    }

    public static void init() {
        CakeVmMemoryService cakeVmMemoryService = new CakeVmMemoryService();

        cakeVmMemoryService.memoryTable = new HashMap<>();

        instance = cakeVmMemoryService;
    }

    public static CakeVmMemoryService getInstance() {
        return instance;
    }

}
