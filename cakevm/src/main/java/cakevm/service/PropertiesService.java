package cakevm.service;

import cakevm.constant.SystemProperties;
import cakevm.exception.PropertiesVerificationException;
import cakevm.util.PropertiesUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PropertiesService {

    private static final List<String> PROPERTIES = new ArrayList<>();

    private static PropertiesService instance;

    private Map<String, String> propertyMap;

    static {
        PROPERTIES.add(SystemProperties.CONFIG_PATH_PROPERTY);
        PROPERTIES.add(SystemProperties.CONFIG_SCHEMA_PATH_PROPERTY);
    }

    public String getProperty(String propertyTitle) {
        String nullableValue = propertyMap.get(propertyTitle);
        String propertyNotExistsMessage = PropertiesUtils.getPropertyNotExistsMessage(propertyTitle);
        return Optional.ofNullable(nullableValue).orElseThrow(() -> new RuntimeException(propertyNotExistsMessage));
    }

    private void verifyProperties() throws PropertiesVerificationException {
        List<String> exceptionMessages = PROPERTIES.stream()
                .filter(property -> !PropertiesUtils.isPropertyExist(property))
                .map(PropertiesUtils::getPropertyNotExistsMessage)
                .toList();

        if (!exceptionMessages.isEmpty()) {
            throw new PropertiesVerificationException(exceptionMessages);
        }
    }

    private void populatePropertiesMap() {
        this.propertyMap = PROPERTIES.stream()
                .collect(Collectors.toMap(Function.identity(), PropertiesUtils::getSystemPropertyStrict));
    }

    public static void init() {
        PropertiesService propertiesService = new PropertiesService();
        propertiesService.verifyProperties();
        propertiesService.populatePropertiesMap();

        instance = propertiesService;
    }

    public static PropertiesService getInstance() {
        return instance;
    }

}
