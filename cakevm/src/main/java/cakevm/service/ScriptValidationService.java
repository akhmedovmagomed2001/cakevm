package cakevm.service;

import cakevm.exception.ScriptValidationServiceException;
import cakevm.model.CakeVmCommand;
import cakevm.util.StatementUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ScriptValidationService {

    private static final Logger LOG = LoggerFactory.getLogger(ScriptValidationService.class);

    private CakeVmConfigService cakeVmConfigService;

    private static ScriptValidationService instance;

    public void validateScript(String formattedScriptFilepath) {
        File formatterScriptFile = new File(formattedScriptFilepath);
        try (Scanner scanner = new Scanner(formatterScriptFile)) {
            String statementDelimiter = ";";
            scanner.useDelimiter(statementDelimiter);

            int lineCounter = -1;

            while (scanner.hasNext()) {
                lineCounter++;
                String statement = scanner.next().trim();

                if (statement.isBlank()) {
                    continue;
                }

                validateStatement(statement, lineCounter);
            }
        } catch (FileNotFoundException ex) {
            throw new ScriptValidationServiceException(ex);
        }

        LOG.info("The cakevm script is valid");
    }

    private void validateStatement(String statement, int lineNumber) {
        List<String> words = StatementUtils.extractWords(statement);

        String command = words.get(0);

        boolean isCommandExists = cakeVmConfigService.isCommandExists(command);
        if (!isCommandExists) {
            String errorMessagePattern = "Line %d: Command '%s' is not defined";
            String errorMessage = String.format(errorMessagePattern, lineNumber, command);
            throw new ScriptValidationServiceException(errorMessage);
        }

        CakeVmCommand commandData = cakeVmConfigService.getCommandByTitle(command);
        int argsCount = commandData.getArgsCount();
        List<String> commandArgs = words.subList(1, words.size());
        if (commandArgs.size() != argsCount) {
            String errorMessagePattern = "Line %d: Command '%s' accepts '%d' arguments, but '%d' are provided. Input args: %s";
            String errorMessage = String.format(errorMessagePattern, lineNumber, command, argsCount, commandArgs.size(), commandArgs);
            throw new ScriptValidationServiceException(errorMessage);
        }
    }

    public static void init() {
        ScriptValidationService scriptValidationService = new ScriptValidationService();

        scriptValidationService.cakeVmConfigService = CakeVmConfigService.getInstance();

        instance = scriptValidationService;
    }

    public static ScriptValidationService getInstance() {
        return instance;
    }
}
