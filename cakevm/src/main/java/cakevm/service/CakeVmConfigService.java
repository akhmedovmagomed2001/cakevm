package cakevm.service;

import cakevm.constant.SystemProperties;
import cakevm.exception.InvalidConfigFormatException;
import cakevm.model.CakeVmCommand;
import cakevm.model.CakeVmConfig;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CakeVmConfigService {

    private PropertiesService propertiesService;
    private ConfigValidationService configValidationService;

    private CakeVmConfig cakeVmConfig;
    private Set<String> commands;
    private Map<String, CakeVmCommand> commandsMap;

    private static CakeVmConfigService instance;

    public boolean isCommandExists(String command) {
        return commands.contains(command);
    }

    public CakeVmCommand getCommandByTitle(String command) {
        CakeVmCommand cakeVmCommandNullable = commandsMap.get(command);
        return Optional.ofNullable(cakeVmCommandNullable).orElseThrow();
    }

    private void parseConfig() {
        configValidationService.validateScriptConfigs();

        String configFilepath = propertiesService.getProperty(SystemProperties.CONFIG_PATH_PROPERTY);
        File configFile = new File(configFilepath);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(CakeVmConfig.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            this.cakeVmConfig = (CakeVmConfig) jaxbUnmarshaller.unmarshal(configFile);
        } catch (JAXBException ex) {
            throw new InvalidConfigFormatException(ex);
        }
    }

    private void setupCommands() {
        commands = cakeVmConfig.getCommands()
                .stream()
                .map(CakeVmCommand::getTitle)
                .collect(Collectors.toSet());
    }

    private void setupCommandsMap() {
        commandsMap = cakeVmConfig.getCommands()
                .stream()
                .collect(Collectors.toMap(CakeVmCommand::getTitle, Function.identity()));
    }

    public static void init() {
        CakeVmConfigService cakeVmConfigService = new CakeVmConfigService();

        cakeVmConfigService.propertiesService = PropertiesService.getInstance();
        cakeVmConfigService.configValidationService = ConfigValidationService.getInstance();

        cakeVmConfigService.parseConfig();
        cakeVmConfigService.setupCommands();
        cakeVmConfigService.setupCommandsMap();

        instance = cakeVmConfigService;
    }

    public static CakeVmConfigService getInstance() {
        return instance;
    }

}
