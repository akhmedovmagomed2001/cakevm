package cakevm.service;

import cakevm.constant.SystemProperties;
import cakevm.exception.InvalidConfigFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigValidationService {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigValidationService.class);

    private PropertiesService propertiesService;
    private XmlXsdValidatorService xmlXsdValidatorService;

    private static ConfigValidationService instance;

    public void validateScriptConfigs() {
        String configFilepath = propertiesService.getProperty(SystemProperties.CONFIG_PATH_PROPERTY);
        String configSchemaFilepath = propertiesService.getProperty(SystemProperties.CONFIG_SCHEMA_PATH_PROPERTY);

        boolean xmlFileValid = xmlXsdValidatorService.isXmlFileValid(configSchemaFilepath, configFilepath);
        if (!xmlFileValid) {
            String errorMessage = "The input config is invalid";
            throw new InvalidConfigFormatException(errorMessage);
        }

        LOG.info("The cakevm configs are valid");
    }

    public static void init() {
        ConfigValidationService cakeVmConfigService = new ConfigValidationService();

        cakeVmConfigService.propertiesService = PropertiesService.getInstance();
        cakeVmConfigService.xmlXsdValidatorService = XmlXsdValidatorService.getInstance();

        instance = cakeVmConfigService;
    }

    public static ConfigValidationService getInstance() {
        return instance;
    }

}
