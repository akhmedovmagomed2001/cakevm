package cakevm.service;

import cakevm.exception.FormatServiceException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FormatService {

    private static FormatService instance;

    public void formatScript(String inputFilePath, String outputFilePath) {
        File inputFile = new File(inputFilePath);
        File outputFile = new File(outputFilePath);

        try {
            outputFile.createNewFile();
        } catch (IOException ex) {
            throw new FormatServiceException(ex);
        }

        try (
                Scanner scanner = new Scanner(inputFile);
                FileWriter fileWriter = new FileWriter(outputFile)
        ) {

            String statementDelimiter = ";";
            scanner.useDelimiter(statementDelimiter);
            while (scanner.hasNextLine()) {
                String line = scanner.next();
                processStatement(line, fileWriter);
            }

            fileWriter.write("\n");
        } catch (IOException ex) {
            throw new FormatServiceException(ex);
        }
    }

    public String formatScript(String scriptFilepath) {
        String outputFilepathFormat = "%s_formatted";
        String outputFilepath = String.format(outputFilepathFormat, scriptFilepath);

        File outputFile = new File(outputFilepath);
        if (outputFile.exists()) {
            outputFile.delete();
        }

        formatScript(scriptFilepath, outputFilepath);

        return outputFilepath;
    }

    private void processStatement(String statement, FileWriter fileWriter) throws IOException {
        if (statement.isBlank()) {
            return;
        }
        String fullStatementPattern = "%s;\n";

        String cleanedStatement = cleanStatement(statement);
        String fullStatement = String.format(fullStatementPattern, cleanedStatement);
        fileWriter.write(fullStatement);
    }

    private String cleanStatement(String statement) {
        return statement.trim()
                .replace("\n", " ")
                .replaceAll(" +", " ");
    }

    public static void init() {
        instance = new FormatService();
    }

    public static FormatService getInstance() {
        return instance;
    }
}
