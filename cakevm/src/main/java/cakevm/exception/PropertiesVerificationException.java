package cakevm.exception;

import java.util.List;

public class PropertiesVerificationException extends RuntimeException {
    private final List<String> exceptionMessages;

    public PropertiesVerificationException(List<String> exceptionMessages) {
        super();
        this.exceptionMessages = exceptionMessages;
    }

    @Override
    public String getMessage() {
        StringBuilder messageBuilder = new StringBuilder();

        exceptionMessages.forEach(message -> messageBuilder.append(message).append('\n'));
        messageBuilder.append('\n');

        String suggestionMessage = "Please enter mentioned properties as a system property\n";
        String exampleMessage = "Example:\njava -DpropertyName=value -jar jarName";
        messageBuilder.append(suggestionMessage).append(exampleMessage);

        return messageBuilder.toString();
    }

}
