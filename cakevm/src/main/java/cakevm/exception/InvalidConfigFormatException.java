package cakevm.exception;

public class InvalidConfigFormatException extends RuntimeException {
    public InvalidConfigFormatException(String message) {
        super(message);
    }

    public InvalidConfigFormatException(Throwable cause) {
        super(cause);
    }
}
