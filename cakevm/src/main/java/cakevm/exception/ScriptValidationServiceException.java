package cakevm.exception;

public class ScriptValidationServiceException extends RuntimeException {
    public ScriptValidationServiceException(Throwable cause) {
        super(cause);
    }

    public ScriptValidationServiceException(String message) {
        super(message);
    }
}
