package cakevm.exception;

public class ModeOptionsValidationException extends RuntimeException {
    public ModeOptionsValidationException(String message) {
        super(message);
    }
}
