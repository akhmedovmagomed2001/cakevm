package cakevm.exception;

public class FormatCommandException extends RuntimeException {
    public FormatCommandException(String message) {
        super(message);
    }
}
