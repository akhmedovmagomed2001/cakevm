package cakevm.exception;

public class ModeValidationException extends RuntimeException {
    public ModeValidationException(String message) {
        super(message);
    }
}
