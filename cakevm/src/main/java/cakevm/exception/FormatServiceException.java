package cakevm.exception;

public class FormatServiceException extends RuntimeException {
    public FormatServiceException(Throwable cause) {
        super(cause);
    }
}
