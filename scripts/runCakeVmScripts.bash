
# Call with format mode
java \
-DcakeConfigPath=$CFG_PATH/cakevm-config.xml \
-DcakeConfigSchemaPath=$CFG_SCHEMA_PATH/cakevm-config-schema.xsd \
-jar $JAR_PATH/cakevm.jar \
f \
-i $SRC_PATH/src3_format.cake \
-o $OUT_PATH/src3_format.cake_out



# Call with validation mode
java \
-DcakeConfigPath=$CFG_PATH/cakevm-config.xml \
-DcakeConfigSchemaPath=$CFG_SCHEMA_PATH/cakevm-config-schema.xsd \
-jar $JAR_PATH/cakevm.jar \
v \
-i $SRC_PATH/src3_valid.cake



# Call with validation mode
java \
-DcakeConfigPath=$CFG_PATH/cakevm-config.xml \
-DcakeConfigSchemaPath=$CFG_SCHEMA_PATH/cakevm-config-schema.xsd \
-jar $JAR_PATH/cakevm.jar \
e \
-i $SRC_PATH/src3_valid.cake \
-o $OUT_PATH/src3_format.cake_out
