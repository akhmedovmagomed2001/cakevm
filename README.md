# CakeVM

CakeVM is a virtual machine.

### Cake script
To interact with it, the *cake script* is needed to be written.
*Cake scripts* usually ends up with `.cake` extention

### Modules
1. Formatter
2. Config Validator
3. Code Validator
4. Compiler
5. VM

### Config
Configs setup the information about operations, that can be used in the cake script.

The config is in the XML format.

The config should follow the coresponding XML schema difinitaion. 

